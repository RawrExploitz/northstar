package me.tojatta.northstar.utils;

import com.mojang.authlib.Agent;
import com.mojang.authlib.UserAuthentication;
import com.mojang.authlib.exceptions.AuthenticationException;
import com.mojang.authlib.yggdrasil.YggdrasilAuthenticationService;
import net.minecraft.util.Session;

import java.net.Proxy;
import java.util.UUID;

/**
 * Created by Tojatta on 2/16/2016.
 */
public class Login {

    /**
     * Minecraft session authentication method.
     * @param username
     * @param password
     * @return
     */
    public static Session authenticate(String username, String password) {
        YggdrasilAuthenticationService yggdrasilAuthenticationService = new YggdrasilAuthenticationService(Proxy.NO_PROXY, UUID.randomUUID().toString());
        UserAuthentication userAuthentication = yggdrasilAuthenticationService.createUserAuthentication(Agent.MINECRAFT);
        userAuthentication.setUsername(username);
        userAuthentication.setPassword(password);
        try {
            userAuthentication.logIn();
            return new Session(userAuthentication.getSelectedProfile().getName(), userAuthentication.getSelectedProfile().getId().toString(), userAuthentication.getAuthenticatedToken(), username.contains("@") ? "mojang" : "legacy");
        } catch (AuthenticationException e) {
            e.printStackTrace();
        }
        return null;
    }
}
