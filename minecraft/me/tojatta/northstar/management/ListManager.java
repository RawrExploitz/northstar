package me.tojatta.northstar.management;

import java.util.List;

/**
 * Created by Tojatta on 2/16/2016.
 */
public abstract class ListManager<T> {

    /**
     * List that holds T.
     */
    public List<T> contents;

    /**
     * Abstract setup method for initializing List<T> contents
     */
    public abstract void setup();

    /**
     * Returns contents of the list.
     * @return
     */
    public List<T> getContents() {
        return contents;
    }

    /**
     * Adds 't' to contents list.
     * @param t
     */
    public void add(T t) {
        contents.add(t);
    }

    /**
     * Removes 't' from contents list.
     * @param t
     * @return
     */
    public boolean remove(T t) {
        return contents.remove(t);
    }
}
