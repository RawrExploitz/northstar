package me.tojatta.northstar;

import net.minecraft.client.Minecraft;

/**
 * Created by Tojatta on 2/16/2016.
 */
public class Northstar {

    /**
     * Hack name.
     */
    public static final String HACKNAME = "Northstar";

    /**
     * Hack build.
     */
    public static final String BUILD = "160216";

    /**
     * Hack developers.
     */
    public static final String[] DEVS = {"Tojatta"};

    /**
     * Northstar constructor.
     * @param mc
     */
    public Northstar(Minecraft mc){

    }



}
